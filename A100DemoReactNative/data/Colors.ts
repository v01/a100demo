enum Colors {
    Red = '#FF0000',
    Yellow = '#FFFF00',
    Green = '#0da50f',
    Grey = '#999',
    TRANS = 'transparent'
}

export default Colors;
