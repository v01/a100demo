import IDocData from "../models/IDocData";
import Images from "./Photos";

const DocsData: Array<IDocData> = [
    {
        fileName: Images.files.gost55525.fileName,
        fileSize: '1054KB',
        filePath: Images.files.gost55525.path,
        url: 'https://drive.google.com/open?id=1_45BEHD9XBsZNi3odwRGJDaTEoGzYE-f',
    },
    {
        fileName: Images.files.gost57381.fileName,
        fileSize: '1315KB',
        filePath: Images.files.gost57381.path,
        url: 'https://drive.google.com/open?id=14ojENCY-QSGWxvOBygCjnfiWfTu_KvQx'
    },
    {
        fileName: Images.files.a100inst.fileName,
        fileSize: '5774KB',
        filePath: Images.files.a100inst.path,
        url: 'https://drive.google.com/open?id=1-ulnFtRbgFAgRERjZJJWCwr-j9dPTgRx'
    },
    {
        fileName: Images.files.installInstruc.fileName,
        fileSize: '58KB',
        filePath: Images.files.installInstruc.path,
        url: 'https://drive.google.com/open?id=1SxfcZLMC4h6PvOYiaH3adswJeIDdn-5C'
    },
    {
        fileName: Images.files.otchet.fileName,
        fileSize: '12515KB',
        filePath: Images.files.otchet.path,
        url: 'https://drive.google.com/open?id=1OVSs2icI9L1d1zdaW9A0xo1UB1Qxd_EY'
    },
] 

export default DocsData;
