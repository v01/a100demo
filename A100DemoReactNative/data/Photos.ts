const Images = {
    first: require('../assets/first.jpg'),
    second: require('../assets/second.jpg'),
    third: require('../assets/third.jpg'),
    vik1: require('../assets/vik1.jpg'),
    vik2: require('../assets/vik2.jpg'),
    vik3: require('../assets/vik3.jpg'),
    vik4: require('../assets/vik4.jpg'),
    vik5: require('../assets/vik5.jpg'),
    vik6: require('../assets/vik6.jpg'),
    specifications: {
        st1: require('../assets/st1.png'),
        st2: require('../assets/st2.png'),
        st3: require('../assets/st3.png'),
    },
    files: {
        gost55525: {
            fileName: 'ГОСТ Р 55525-2017_Стеллажи_Общие_технические_условия.pdf',
            path: '../assets/gost55525.pdf'
        },
        gost57381: {
            fileName: 'ГОСТ Р 57381-2017 Полочные стеллажи.pdf',
            path: '../assets/gost57381.pdf'
        },
        installInstruc: {
            fileName: 'Краткая инструкция по установке клиентского приложения А100.docx',
            path: '../assets/gost57381.pdf'
        },
        a100inst: {
            fileName: 'Руководство планшетной версии А100 для клиентов.docx',
            path: '../assets/a100Inst.docx'
        },
        otchet: {
            fileName: 'Отчет.pdf',
            path: '../assets/otchet.pdf'
        },
    }
}

export default Images;
// ГОСТ Р 55525-2017_Стеллажи_Общие_технические_условия.pdf
// ГОСТ Р 57381-2017 Полочные стеллажи.pdf
// Краткая инструкция по установке клиентского приложения А100.docx
// Руководство планшетной версии А100 для клиентов.docx
// Отчет.pdf