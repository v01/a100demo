interface IDocData {
    fileName: string,
    fileSize: string,
    filePath: string,
    file?: any,
    url: string,
}

export default IDocData;
